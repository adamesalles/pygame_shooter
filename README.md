# Informações do Grupo

## Integrantes
 - Eduardo Adame Salles
 - Ana Carolina Erthal
 - Vinicius Hedler
 - Rodrigo Pintucci
 - Tiago Barradas

## Branch
 - game

## Como trabalhar

1. Apagar o fork do <https://github.com/fccoelho/curso_pygame>
2. Fazer o fork desse repositório
3. Clonar seu fork junto a branch game `git clone -b game <url-seu-fork>`
4. Fazer as alterações + commits
5. Fazer Push para o seu fork (na branch game)
6. Abrir Pull Request para este repositório
7. Pedir Revisão para um dos integrantes
8. Lembrar de atualizar seu fork antes das alterações

## Objetivos Iniciais
 - [x] Traduzir Código (Carol)
 - [x] Revisar e Documentar o Código (Vini)
 - [x] Tamanho da Tela (Edu)
 - [x] Adicionar movimentação (Edu)
 - [ ] Calibrar movimentação (ir de um lado para o outro da tela) (Edu) ~~ Commit parcial / movimentação funcionando, mas não de forma ideal. Existe algo "grudando" o sprite na esquerda e em cima.
 - [ ] Sprites (speed, escudo, etc) (Diguin)
 - [ ] Implementar Sprites (Carol)
 - [ ] Power-Ups (Edu/Vini)
 - [ ] Indicação visual (número de vidas, inimigos) (Tiago)
 - [ ] Músicas e Efeitos Sonoros (Tiago)
 - [ ] Efeitos Visuais (Carol)
 - [ ] Menu (Vini)
 - [ ] Inimigos/Dificuldade/Visual diferente (Diguin/Vini)

## Objetivos Extras
 - [ ] Dois jogadores


---

# Pré-requisitos
Antes de começar este curso você precisa ter instalado em seu computador o git (com o qual irá clora este repositório) e o pygame

```bash
pip install pygame
```

# O Jogo
O jogo que iremos criar chama-se "Corona Shooter". e trata-se de um jogo de naves espaciais inspirado em uma pandemia de coronavirus.

